package insertionAll;
import jpaentity.Node;
import jpaentity.Relation;
import jpaentity.Way;
import javax.xml.bind.JAXBElement;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

public class CopyJAXBElementToEntity {
    public static Node getNodeEntity(JAXBElement<org.openstreetmap.osm._0.Node> node){
        Node temp = new Node();
        Map<String,String> tempTag = new HashMap<>();
        node.getValue().getTag().forEach(value -> tempTag.put(value.getK(),value.getV()));
        temp.setTags(tempTag);
        System.out.println(node.getValue().getUser());
        System.out.println(node.getValue().getChangeset());
        temp.setChangeset(node.getValue().getChangeset().longValue());
        if (node.getValue().isVisible()!=null) {
            temp.setVisible((node.getValue().isVisible() == true) ? new BigInteger("1") : new BigInteger("0"));
        }
            temp.setVersion(node.getValue().getVersion().longValue());
        temp.setLat(node.getValue().getLat());
        temp.setLon(node.getValue().getLon());
        temp.setMyuser(node.getValue().getUser());
        temp.setUid(node.getValue().getUid().longValue());

        Timestamp timestamp = new Timestamp(node.getValue().getTimestamp().toGregorianCalendar().getTimeInMillis());
        temp.setTimestamp(timestamp);
        return  temp;
    }

    public static Way getWayEntity(JAXBElement<org.openstreetmap.osm._0.Way> way){
        Way temp = new Way();
        Map<String,String> tempTag = new HashMap<>();
        way.getValue().getTag().forEach(value -> tempTag.put(value.getK(),value.getV()));
        temp.setTags(tempTag);
        Map<String,String> tempNd = new HashMap<>();
        way.getValue().getNd().forEach(value -> tempNd.put(String.valueOf(value.getRef().hashCode()),value.getRef().toString()));
        temp.setNds(tempNd);
        temp.setChangeset(way.getValue().getChangeset().longValue());
        temp.setVisible((way.getValue().isVisible() == true)? new BigInteger("1"): new BigInteger("0"));
        temp.setVersion(way.getValue().getVersion().longValue());
        temp.setMyuser(way.getValue().getUser());
        temp.setUid(way.getValue().getUid().longValue());
        Timestamp timestamp = new Timestamp(way.getValue().getTimestamp().toGregorianCalendar().getTimeInMillis());
        temp.setTimestamp(timestamp);
        return temp;
    }
    public static Relation getRelationEntity(JAXBElement<org.openstreetmap.osm._0.Relation> relation){
        Relation temp = new Relation();
        Map<String,String> tempTag = new HashMap<>();
        relation.getValue().getTag().forEach(value -> tempTag.put(value.getK(),value.getV()));
        temp.setTags(tempTag);
        Map<String,String> tempMem = new HashMap<>();
        relation.getValue().getMember().forEach(value -> tempMem.put(value.getRef().toString()+value.getType(),value.getRole()));
        temp.setMembers(tempMem);
        temp.setChangeset(relation.getValue().getChangeset().longValue());
        temp.setVisible((relation.getValue().isVisible() == true)? new BigInteger("1"): new BigInteger("0"));
        temp.setVersion(relation.getValue().getVersion().longValue());
        temp.setMyuser(relation.getValue().getUser());
        temp.setUid(relation.getValue().getUid().longValue());
        Timestamp timestamp = new Timestamp(relation.getValue().getTimestamp().toGregorianCalendar().getTimeInMillis());
        temp.setTimestamp(timestamp);
        return temp;
    }
}
