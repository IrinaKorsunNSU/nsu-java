package insertionAll;

import entityRepository.NodeRepository;
import entityRepository.RelationRepository;
import entityRepository.WayRepository;
import jpaentity.Node;
import jpaentity.Relation;
import jpaentity.Way;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.jms.JndiConnectionFactoryAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Collections;
import java.util.List;


@EnableAutoConfiguration(exclude = {JndiConnectionFactoryAutoConfiguration.class,DataSourceAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class,JpaRepositoriesAutoConfiguration.class,DataSourceTransactionManagerAutoConfiguration.class})
@Component

public class RunFillingDatabase {
    @Autowired(required = true)
    private NodeRepository noderep;
    @Autowired(required = true)
    private WayRepository wayrep;
    @Autowired(required = true)
    private RelationRepository relrep;

    public void addNode(Node node) {
        noderep.save(node);
    }
    public void addWay(Way way) {
        wayrep.save(way);
    }
    public  void addRel(Relation rel){
        relrep.save(rel);
    }

public static void main(String [] argc) throws FileNotFoundException, XMLStreamException, JAXBException {
    java.util.logging.Logger.getLogger("org.hibernate").setLevel(java.util.logging.Level.OFF);
    List<Logger> loggers = Collections.<Logger>list(LogManager.getCurrentLoggers());
    loggers.add(LogManager.getRootLogger());
    for ( Logger logger : loggers ) {
        logger.setLevel(Level.OFF);
    }
    ApplicationContext ctx=new ClassPathXmlApplicationContext("applicationContext.xml");
    System.out.println("ctx>>"+ctx);
    RunFillingDatabase myDemo=ctx.getBean(RunFillingDatabase.class);


    XMLInputFactory xmlif = XMLInputFactory.newInstance();
    XMLStreamReader xmlr = xmlif
            .createXMLStreamReader(new FileReader("RU-NVS.osm"));

    JAXBContext ucontext = JAXBContext.newInstance(org.openstreetmap.osm._0.Node.class);
    Unmarshaller unmarshaller = ucontext.createUnmarshaller();

    JAXBContext wcontext = JAXBContext.newInstance(org.openstreetmap.osm._0.Way.class);
    Unmarshaller wunmarshaller = wcontext.createUnmarshaller();

    JAXBContext rcontext = JAXBContext.newInstance(org.openstreetmap.osm._0.Relation.class);
    Unmarshaller runmarshaller = rcontext.createUnmarshaller();

    try {
        xmlr.nextTag();
        xmlr.nextTag();
        while (xmlr.getEventType() == XMLStreamConstants.START_ELEMENT) {
            if (xmlr.getLocalName().equals("node")) {
                System.out.println("node");
                JAXBElement<org.openstreetmap.osm._0.Node> node = unmarshaller.unmarshal(xmlr, org.openstreetmap.osm._0.Node.class);

                myDemo.addNode(CopyJAXBElementToEntity.getNodeEntity(node));
            }
            else
            if(xmlr.getLocalName().equals("way")) {
                JAXBElement<org.openstreetmap.osm._0.Way> way = wunmarshaller.unmarshal(xmlr, org.openstreetmap.osm._0.Way.class);
                myDemo.addWay(CopyJAXBElementToEntity.getWayEntity(way));
            }
            else
            if(xmlr.getLocalName().equals("relation")) {
                JAXBElement<org.openstreetmap.osm._0.Relation> rel = runmarshaller.unmarshal(xmlr, org.openstreetmap.osm._0.Relation.class);
                myDemo.addRel(CopyJAXBElementToEntity.getRelationEntity(rel));
            }
            else {
                unmarshaller.unmarshal(xmlr, org.openstreetmap.osm._0.Node.class);
            }
            if (xmlr.getEventType() == XMLStreamConstants.CHARACTERS) {
                xmlr.next();
            }
        }
    } finally {
        xmlr.close();
    }
}
}
