package entityRepository;

import jpaentity.Node;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Repository
@Service("nodeService")
public interface NodeRepository extends JpaRepository<Node, Long> {

}
