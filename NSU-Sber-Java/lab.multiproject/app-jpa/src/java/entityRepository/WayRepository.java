package entityRepository;


import jpaentity.Way;
import org.springframework.data.jpa.repository.JpaRepository;


public interface WayRepository extends JpaRepository<Way, Long> {
}
