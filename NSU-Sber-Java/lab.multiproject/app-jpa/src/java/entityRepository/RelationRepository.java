package entityRepository;

import jpaentity.Relation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface RelationRepository  extends JpaRepository<Relation, Long> {
}
