package jpaentity;


import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(name = "way", schema = "public", catalog = "postgres")
@TypeDefs({
        @TypeDef(name = "hstore",  typeClass = HstoreUserType.class)
})
public class Way {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",unique=true, nullable = false)
    private Long id;
    private String myuser;
    private Long uid;
    private BigInteger visible;
    private Long version;
    private Long changeset;
    private Timestamp timestamp;
    @Type(type = "hstore")
    @Column(name= "tag", columnDefinition = "hstore")
    private Map<String, String> tags = new HashMap<>();

    @Type(type = "hstore")
    @Column(name= "nd", columnDefinition = "hstore")
    private Map<String, String> nds = new HashMap<>();

    @Column(name = "tag")
    public Map<String, String> getTags() {
        return tags;
    }

    public void setTags(Map<String, String> tags) {
        this.tags = tags;
    }
    @Column(name = "nd")
    public Map<String, String> getNds() {
        return nds;
    }

    public void setNds(Map<String, String> nds) {
        this.nds = nds;
    }

    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "myuser")
    public String getMyuser() {
        return myuser;
    }

    public void setMyuser(String myuser) {
        this.myuser = myuser;
    }

    @Column(name = "uid")
    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    @Column(name = "visible")
    public BigInteger getVisible() {
        return visible;
    }

    public void setVisible(BigInteger visible) {
        this.visible = visible;
    }

    @Column(name = "version")
    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Column(name = "changeset")
    public Long getChangeset() {
        return changeset;
    }

    public void setChangeset(Long changeset) {
        this.changeset = changeset;
    }

    @Column(name = "timestamp")
    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

}
