DROP TABLE  table_users;
CREATE TABLE table_users
(
  id DECIMAL PRIMARY KEY ,
  username VARCHAR(50),
  count DECIMAL
);
CREATE EXTENSION hstore;
CREATE TABLE Node (
  id SERIAL  primary key,
  lat DOUBLE PRECISION ,
  lon DOUBLE PRECISION ,
  myuser VARCHAR (255) ,
  uid BIGINT CHECK ( uid >= 0 ) ,
  visible  INT,
  version BIGINT CHECK ( version >= 0 ) ,
  changeset BIGINT CHECK ( changeset >= 0 ) ,
  timestamp TIMESTAMP,
  tag hstore
);
CREATE TABLE Relation (
  id SERIAL  primary key ,
  myuser VARCHAR (255) ,
  uid BIGINT CHECK ( uid >= 0 ) ,
  visible INT,
  version BIGINT CHECK ( version >= 0 ) ,
  changeset BIGINT CHECK ( changeset >= 0 ) ,
  "timestamp" TIMESTAMP,
  tag hstore,
  member hstore
);
  CREATE TABLE Way (
    id SERIAL  primary key ,
    myuser VARCHAR (255) ,
    uid BIGINT CHECK ( uid >= 0 ) ,
    visible INT ,
    version BIGINT CHECK ( version >= 0 ) ,
    changeset BIGINT CHECK ( changeset >= 0 ) ,
    "timestamp" TIMESTAMP,
    tag hstore,
    nd hstore
  )