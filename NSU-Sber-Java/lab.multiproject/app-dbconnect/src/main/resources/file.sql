DROP TABLE IF EXISTS member CASCADE;
DROP TABLE IF EXISTS nd CASCADE;
DROP TABLE IF EXISTS tag CASCADE;
DROP TABLE IF EXISTS relation CASCADE;
DROP TABLE IF EXISTS way CASCADE;
DROP TABLE IF EXISTS node CASCADE;
DROP TABLE IF EXISTS bounds CASCADE;
DROP TABLE IF EXISTS osm CASCADE;

CREATE TABLE osm (
	document_id TEXT ,
	osm_id BIGINT CHECK ( osm_id >= 0 )PRIMARY KEY ,
	bounds_id BIGINT CHECK ( bounds_id >= 0 ) ,
	node_id BIGINT CHECK ( node_id >= 0 ) ,
	way_id BIGINT CHECK ( way_id >= 0 ) ,
	relation_id BIGINT CHECK ( relation_id >= 0 ) ,
	version REAL CHECK ( version = 0.6 ) ,
	generator TEXT CHECK ( generator = 'CGImap 0.0.2' ) 
);

CREATE TABLE bounds (
	document_id TEXT ,
	bounds_id BIGINT CHECK ( bounds_id >= 0 ) PRIMARY KEY ,
	osm_id BIGINT CHECK ( osm_id >= 0 ) ,
	minlat DOUBLE PRECISION ,
	minlon DOUBLE PRECISION ,
	maxlat DOUBLE PRECISION ,
	maxlon DOUBLE PRECISION 
);

CREATE TABLE node (
	document_id TEXT ,
	node_id BIGINT CHECK ( node_id >= 0 ) PRIMARY KEY ,
	osm_id BIGINT CHECK ( osm_id >= 0 ) ,
	tag_id BIGINT CHECK ( tag_id >= 0 ) ,
	id BIGINT CHECK ( id >= 0 ) ,
	lat DOUBLE PRECISION ,
	lon DOUBLE PRECISION ,
	"user" TEXT ,
	uid BIGINT CHECK ( uid >= 0 ) ,
	visible  DECIMAL,
	version BIGINT CHECK ( version >= 0 ) ,
	changeset BIGINT CHECK ( changeset >= 0 ) ,
	"timestamp" TIMESTAMP 
);

CREATE TABLE way (
	document_id TEXT ,
	way_id BIGINT CHECK ( way_id >= 0 ) PRIMARY KEY,
	osm_id BIGINT CHECK ( osm_id >= 0 ) ,
	nd_id BIGINT CHECK ( nd_id >= 0 ) ,
	tag_id BIGINT CHECK ( tag_id >= 0 ) ,
	id BIGINT CHECK ( id >= 0 ) ,
	"user" TEXT ,
	uid BIGINT CHECK ( uid >= 0 ) ,
	visible DECIMAL ,
	version BIGINT CHECK ( version >= 0 ) ,
	changeset BIGINT CHECK ( changeset >= 0 ) ,
	"timestamp" TIMESTAMP 
);

CREATE TABLE relation (
	document_id TEXT ,
	relation_id BIGINT CHECK ( relation_id >= 0 ) PRIMARY KEY,
	osm_id BIGINT CHECK ( osm_id >= 0 ) ,
	member_id BIGINT CHECK ( member_id >= 0 ) ,
	tag_id BIGINT CHECK ( tag_id >= 0 ) ,
	id BIGINT CHECK ( id >= 0 ) ,
	"user" TEXT ,
	uid BIGINT CHECK ( uid >= 0 ) ,
	visible DECIMAL ,
	version BIGINT CHECK ( version >= 0 ) ,
	changeset BIGINT CHECK ( changeset >= 0 ) ,
	"timestamp" TIMESTAMP 
);

CREATE TABLE tag (
	document_id TEXT ,
	tag_id BIGINT CHECK ( tag_id >= 0 ) PRIMARY KEY ,
	node_id BIGINT CHECK ( node_id >= 0 ) ,
	k TEXT ,
	v TEXT ,
	way_id BIGINT CHECK ( way_id >= 0 ) ,
	relation_id BIGINT CHECK ( relation_id >= 0 ) 
);


CREATE TABLE nd (
	document_id TEXT ,
	nd_id BIGINT CHECK ( nd_id >= 0 ) PRIMARY KEY ,
	way_id BIGINT CHECK ( way_id >= 0 ) ,
	ref BIGINT CHECK ( ref >= 0 ) 
);


DROP TYPE IF EXISTS ENUM_member_type;
CREATE TYPE ENUM_member_type AS ENUM ( 'node', 'way', 'relation' );
CREATE TABLE member (
	document_id TEXT ,
	member_id BIGINT CHECK ( member_id >= 0 ) PRIMARY KEY ,
	relation_id BIGINT CHECK ( relation_id >= 0 ) ,
	type ENUM_member_type ,
	ref BIGINT CHECK ( ref >= 0 ) ,
	role TEXT 
);

