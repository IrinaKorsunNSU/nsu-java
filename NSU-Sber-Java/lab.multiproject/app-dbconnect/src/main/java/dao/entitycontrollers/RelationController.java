package dao.entitycontrollers;

import dao.AbstractController;
import org.openstreetmap.osm._0.Relation;

import java.util.List;

/**
 * Created by irina on 24.04.16.
 */
public class RelationController extends AbstractController <Relation,Integer> {
    @Override
    public List<Relation> getAll() {
        return null;
    }

    @Override
    public Relation update(Relation entity) {
        return null;
    }

    @Override
    public Relation getEntityById(Integer id) {
        return null;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    @Override
    public boolean create(Relation entity) {
        return false;
    }
}
