package dao.entitycontrollers;


import dao.AbstractController;
import org.openstreetmap.osm._0.Nd;

import java.util.List;

/**
 * Created by irina on 24.04.16.
 */
public class NdController extends AbstractController<Nd,Integer> {
    @Override
    public List<Nd> getAll() {
        return null;
    }

    @Override
    public Nd update(Nd entity) {
        return null;
    }

    @Override
    public Nd getEntityById(Integer id) {
        return null;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    @Override
    public boolean create(Nd entity) {
        return false;
    }
}
