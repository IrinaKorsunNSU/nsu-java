package dao.entitycontrollers;

import dao.AbstractController;
import org.openstreetmap.osm._0.Node;

import java.util.List;

/**
 * Created by irina on 24.04.16.
 */
public class NodeController extends AbstractController<Node,Integer> {
    @Override
    public List<Node> getAll() {
        return null;
    }

    @Override
    public Node update(Node entity) {
        return null;
    }

    @Override
    public Node getEntityById(Integer id) {
        return null;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    @Override
    public boolean create(Node entity) {
        return false;
    }
}
