package dao.entitycontrollers;
import dao.AbstractController;
import org.openstreetmap.osm._0.Member;

import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by irina on 24.04.16.
 */
public class MemberController extends AbstractController<Member, Integer> {
    public static final String SELECT_ALL_MEMBER = "SELECT * FROM MEMBER";

    @Override
    public List<Member> getAll() {
        List<Member> lst = new LinkedList<>();
        PreparedStatement ps = getPrepareStatement(SELECT_ALL_MEMBER);
        try {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Member member = new Member();
                member.setType(rs.getString(2));
                member.setRole(rs.getString(3));
                member.setRef(BigInteger.valueOf(rs.getInt(4)));
                lst.add(member);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closePrepareStatement(ps);
        }

        return lst;
    }

    @Override
    public Member update(Member entity) {
        return null;
    }

    @Override
    public Member getEntityById(Integer id) {
        return null;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    @Override
    public boolean create(Member entity) {
        return false;
    }

}