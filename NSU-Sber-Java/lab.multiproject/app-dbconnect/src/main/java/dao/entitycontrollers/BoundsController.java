package dao.entitycontrollers;

import dao.AbstractController;
import org.openstreetmap.osm._0.Bounds;

import java.util.List;

/**
 * Created by irina on 26.04.16.
 */
public class BoundsController extends AbstractController<Bounds,Integer> {
    @Override
    public List getAll() {
        return null;
    }

    @Override
    public Bounds update(Bounds entity) {
        return null;
    }

    @Override
    public Bounds getEntityById(Integer id) {
        return null;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    @Override
    public boolean create(Bounds entity) {
        return false;
    }
}

