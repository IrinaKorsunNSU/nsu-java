package dao.entitycontrollers;


import dao.AbstractController;
import org.openstreetmap.osm._0.Osm;

import java.util.List;

/**
 * Created by irina on 24.04.16.
 */
public class OsmController extends AbstractController<Osm,Integer> {
    @Override
    public List<Osm> getAll() {
        return null;
    }

    @Override
    public Osm update(Osm entity) {
        return null;
    }

    @Override
    public Osm getEntityById(Integer id) {
        return null;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    @Override
    public boolean create(Osm entity) {
        return false;
    }
}
