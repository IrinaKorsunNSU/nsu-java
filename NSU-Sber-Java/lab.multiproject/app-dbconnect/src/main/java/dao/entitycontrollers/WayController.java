package dao.entitycontrollers;

import dao.AbstractController;
import org.openstreetmap.osm._0.Way;

import java.util.List;

/**
 * Created by irina on 24.04.16.
 */
public class WayController extends AbstractController<Way,Integer> {
    @Override
    public List<Way> getAll() {
        return null;
    }

    @Override
    public Way update(Way entity) {
        return null;
    }

    @Override
    public Way getEntityById(Integer id) {
        return null;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    @Override
    public boolean create(Way entity) {
        return false;
    }
}
