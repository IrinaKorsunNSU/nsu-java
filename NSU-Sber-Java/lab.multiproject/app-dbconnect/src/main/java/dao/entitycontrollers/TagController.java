package dao.entitycontrollers;


import dao.AbstractController;
import org.openstreetmap.osm._0.Tag;

import java.util.List;

/**
 * Created by irina on 24.04.16.
 */
public class TagController extends AbstractController<Tag,Integer> {
    @Override
    public List<Tag> getAll() {
        return null;
    }

    @Override
    public Tag update(Tag entity) {
        return null;
    }

    @Override
    public Tag getEntityById(Integer id) {
        return null;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    @Override
    public boolean create(Tag entity) {
        return false;
    }
}
