import java.io.*;
import java.sql.*;
import java.util.Properties;
import java.util.logging.Logger;


public class DBConnection {
    private static Connection connection = null;

    public Connection checkConnection(){
        return connection;
    }

    public int getConnection()  {
        Logger log = Logger.getLogger(DBConnection.class.getName());

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
           return 3;
        }
        log.info("Driver connected");
        Properties prop = new Properties();
        InputStream input  = getClass().getResourceAsStream("config.properties");

        try {
            prop.load(input);
        } catch (IOException e) {
            return 1;
        }
        try {
            connection = DriverManager.getConnection(prop.getProperty("url"), prop.getProperty("dbuser"), prop.getProperty("dbpassword"));
        } catch (SQLException e) {
            return 2;
        }
        log.info("Accessed");
        try {
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int resetDatabase(String filepath) {
        String s;
        StringBuffer sb = new StringBuffer();
        ClassLoader classLoader = getClass().getClassLoader();
        FileInputStream fr;
        try {
            fr = new FileInputStream(classLoader.getResource(filepath).getFile());
        } catch (FileNotFoundException e) {
            return 1;
        }
        BufferedReader br = new BufferedReader(new InputStreamReader(fr));

        try {
            while((s = br.readLine()) != null)
            {
                sb.append(s);
            }
        } catch (IOException e) {
            return 2;
        }
        try {
            br.close();
        } catch (IOException e) {
            return 3;
        }
        String[] inst = sb.toString().split(";");
        Statement st;
        try {
            st = connection.createStatement();
        } catch (SQLException e) {
            return 4;
        }

        for(int i = 0; i<inst.length; i++)
            {
                if(!inst[i].trim().equals(""))
                {
                    try {
                        st.executeUpdate(inst[i]);
                    } catch (SQLException e) {
                        return 5;
                    }
                   // System.out.println(">>"+inst[i]);
                }
            }
        return 0;
    }
}
