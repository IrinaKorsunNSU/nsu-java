package insertion;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;


public class StatementInsert {
    Connection dbConnection = null;
    Statement statement = null;
    public StatementInsert(Connection con){
        dbConnection = con;
    }
    public int insert(){
        ClassLoader classLoader = getClass().getClassLoader();
        FileInputStream fis;
        try {
            fis = new FileInputStream(classLoader.getResource("temp.txt").getFile());
        } catch (FileNotFoundException e) {
            return 1;
        }
        BufferedReader br = new BufferedReader(new InputStreamReader(fis));
        String line;
        int i = 1;
        long before = System.currentTimeMillis();


        try {
            while ((line = br.readLine()) != null) {
                //System.out.println(line);
                String [] parts = line.split(":");
                String insertTableSQL = "INSERT INTO table_users"
                        + "(id,username, count) " + "VALUES "
                        + "("+"'"+Integer.toString(i)+"'"+","+"'"+parts[1]+"'"+","+"'"+parts[0]+"'"+");";
                statement = dbConnection.createStatement();
                //System.out.println(insertTableSQL);
                statement.executeUpdate(insertTableSQL);
               // System.out.println("Record is inserted into DBUSER table!");
                i++;
            }
        } catch (IOException e) {
            return 2;
        } catch (SQLException e) {
            return 3;
        }
        long after = System.currentTimeMillis();
        double rez = (double)(i-1)/(double)(after - before);
        System.out.println(rez);

        try {
            br.close();
        } catch (IOException e) {
            return 4;
        }
        return  0;
    }
}
