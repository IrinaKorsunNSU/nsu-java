package insertion;


import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class BatchInsert {
    Connection dbConnection = null;
    public BatchInsert(Connection con){
        dbConnection = con;
    }
    public int insert(){
        ClassLoader classLoader = getClass().getClassLoader();
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(classLoader.getResource("temp.txt").getFile());
        } catch (FileNotFoundException e) {
            return 1;
        }
        BufferedReader br = new BufferedReader(new InputStreamReader(fis));
        String insertTableSQL = "INSERT INTO table_users"
                + "(id,username, count) " + "VALUES "
                + "(?,?,?)";
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
        } catch (SQLException e) {
            return 2;
        }
        String line = null;
        int i = 1;
        long before = System.currentTimeMillis();
        try {
            while ((line = br.readLine()) != null) {
             //   System.out.println(line);
                String [] parts = line.split(":");
                preparedStatement.setInt(1, i);
                preparedStatement.setString(2, parts[1]);
                preparedStatement.setInt(3, Integer.valueOf(parts[0]));
                preparedStatement.addBatch();
                i++;
            }
            int [] numUpdates=preparedStatement.executeBatch();
        } catch (IOException e) {
            return  3;
        } catch (SQLException e) {
            return 4;
        }
        long after = System.currentTimeMillis();
        double rez = (double)(i-1)/(double)(after - before);
        System.out.println(rez);
        return 0;
    }
}
