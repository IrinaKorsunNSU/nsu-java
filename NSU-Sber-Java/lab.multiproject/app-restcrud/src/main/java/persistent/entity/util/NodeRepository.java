package persistent.entity.util;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import persistent.entity.Node;

import java.util.List;


@Repository
@Service("nodeService")
public interface NodeRepository extends JpaRepository<Node, Long> {
    @Query(value = "select * from Node WHERE earth_box(ll_to_earth(:lat,:lon), :rad) > ll_to_earth(lat, lon)", nativeQuery = true)
    List<Node> findInDistance(@Param("lat") double lat, @Param("lon") double lon, @Param("rad") int rad);
}
