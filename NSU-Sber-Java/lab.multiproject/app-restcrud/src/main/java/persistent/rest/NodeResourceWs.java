package persistent.rest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import persistent.entity.Node;
import persistent.entity.util.NodeRepository;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import java.util.List;

@Component
@Path("/nodes")
@Scope("request")
public class NodeResourceWs implements NodeResource{
    @Autowired
    private NodeRepository nodeService;
    @Context
    UriInfo ui;
    private Logger log = Logger.getLogger(this.getClass());

    @GET
    @Produces("application/json")

    public String getNodes() {
        log.info("getNodes entered");
        StringBuffer jsonBuffer = new StringBuffer("[\n");
        List<Node> nodes = nodeService.findAll();
        boolean first = true;
        for (Node node : nodes) {
            if (first)
                first = false;
            else
                jsonBuffer.append(",\n ");
            jsonBuffer.append(getJsonNodeObject(node));
        }
        jsonBuffer.append("]");
        log.info("Sending: \n\n" + jsonBuffer.toString() + "\n\n");
        System.out.println(jsonBuffer.toString());
        return jsonBuffer.toString();
    }

    @GET
    @Path("/{id}")
    @Produces("application/json")
    public String getNode(@PathParam("id") String id) {
        log.info("getNode entered");
        log.info("Hit getNode");
        Node node = nodeService.findOne(Long.parseLong(id));
        String result = (String) getJsonNodeObject(node);
        log.info("Sending: \n\n" + result + "\n\n");
        return result;
    }

    @GET
    @Path("/{rad}/{lat}/{lon}")
    @Produces("application/json")
    public String getNodesInRadius(@PathParam("rad")String rad, @PathParam("lat") String lat, @PathParam("lon") String lon){
        StringBuffer jsonBuffer = new StringBuffer("[\n");
        List<Node> nodes = nodeService.findInDistance(Long.valueOf(lat),Long.valueOf(lon),Integer.valueOf(rad));
        boolean first = true;
        for (Node nod: nodes) {
            if (first) first = false;
            else jsonBuffer.append(",\n ");
            jsonBuffer.append(getJsonNodeObject(nod));
        }
        jsonBuffer.append("]");
        log.info("Sending: \n\n" + jsonBuffer.toString() + "\n\n");
        System.out.println(jsonBuffer.toString());
        return jsonBuffer.toString();
      }

    @Override
    public String updateNode(String id, String name, String age) {
        return null;
    }

    @Override
    public String addNode(String name, String age) {
        return null;
    }

    @Override
    public String deleteNode(String ids) {
        return null;
    }
    private Object getJsonNodeObject(Node node) {
        StringBuffer buffer = new StringBuffer("{\"id\" : " + node.getId());
        buffer.append(" , \"user\" : \"").append(node.getMyuser());
        buffer.append("\" , \"lat\" : \"").append(node.getLat());
        buffer.append("\" , \"lon\" : \"").append(node.getLon());
        buffer.append("} ");
        return buffer.toString();
    }
}
