package persistent.rest;

public interface NodeResource {
    public abstract String getNodes();
    public abstract String getNode(String id);
    public abstract String updateNode( String id, String name, String age);
    public abstract String addNode( String name, String age);
    public abstract String deleteNode(String ids);
}
