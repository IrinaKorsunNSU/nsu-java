package stax;

/**
 * Created by irina on 25.04.16.
 */
import entity.Count;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.*;
import java.util.*;
import java.util.stream.Stream;

public class StAXParser {

    private HashMap<String, Count> users;
    public StAXParser(){
        users = new HashMap<>();
    }

    public HashMap<String,Count> getMap (){
        return users;
    }

    public int parse(String filename) {
        XMLInputFactory xmlif = XMLInputFactory.newInstance();
        ClassLoader classLoader = getClass().getClassLoader();
        XMLStreamReader xmlr = null;
        try {
            xmlr = xmlif.createXMLStreamReader(filename,
            new FileInputStream(classLoader.getResource(filename).getFile()));
        } catch (XMLStreamException e) {
            return 1;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            while(xmlr.hasNext()){
                int eventType = xmlr.next();
                switch (eventType) {
                    case XMLStreamReader.START_ELEMENT:
                        String elementName = xmlr.getLocalName();
                        if (elementName.equals("node")) {
                            readJustUsersInNode(xmlr);
                        }
                        break;
                    case XMLStreamReader.END_ELEMENT:
                        break;
                }
            }
        } catch (XMLStreamException e) {
            return 2;
        }
        return 0;
    }

    private void readJustUsersInNode(XMLStreamReader xmlr) throws XMLStreamException {
        while (xmlr.hasNext()) {
            int eventType = xmlr.getEventType();
            switch (eventType) {
                case XMLStreamReader.START_ELEMENT:
                    for(int i = 0, n = xmlr.getAttributeCount(); i < n; ++i) {
                        QName name = xmlr.getAttributeName(i);
                        String value = xmlr.getAttributeValue(i);
                        if (name.toString().equals("user")){
                            if (users.containsKey(value)){
                                users.get(value).add();
                            }
                            else {
                                users.put(value, new Count(1));
                            }
                        }
                    }
                case XMLStreamReader.END_ELEMENT:
                    return;
            }
        }
    }

    public  <K,V> void printList(Map<K,V> list) {
        try {
            PrintWriter writer = new PrintWriter("temp.txt", "UTF-8");

            for (Map.Entry<K, V> e : list.entrySet()) {
                System.out.println("key is: " + e.getKey() + " value is: " + ((Count) e.getValue()).getA());
                writer.print(((Count) e.getValue()).getA() + ":");
                writer.println(e.getKey());
            }
            writer.flush();
            writer.close();
        } catch (FileNotFoundException e) {

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public <K, V extends Comparable<? super V>> Map<K, V> sortByValue( Map<K, V> map )
    {
        Map<K,V> result = new LinkedHashMap<>();
        Stream<Map.Entry<K,V>> st = map.entrySet().stream();

        st.sorted(Comparator.comparing(e -> e.getValue()))
                .forEach(e ->result.put(e.getKey(),e.getValue()));

        return result;
    }

}
