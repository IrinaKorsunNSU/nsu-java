package marshaller;

import entity.Count;
import entity.Node;
import entity.NodeMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Anatoliy on 03.05.2016.
 */
public class Marshalling {
    private static NodeMap employeeMap;
    private static HashMap<String, Count> map;

    public Marshalling(HashMap<String, Count> mapa) {
        employeeMap = new NodeMap();
        map = mapa;
    }

    public int marshalling() {
        Map<Integer, Node> nodeMap = new HashMap<Integer, Node>();
        Iterator<Map.Entry<String, Count>> entries = map.entrySet().iterator();
        int count = 0;
        while (entries.hasNext()) {
            Map.Entry<String, Count> entry = entries.next();
            nodeMap.put(new Integer(count), new Node(entry.getKey(), entry.getValue()));
            count++;
        }
        employeeMap.setNodeMap(nodeMap);
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(NodeMap.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(employeeMap, new File("nodes.xml"));
        } catch (JAXBException e) {
           return 1;
        }
        return 0;
    }

}
