package entity;

/**
 * Created by irina on 25.04.16.
 */
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashMap;
import java.util.Map;

@XmlRootElement(name="nodes")
@XmlAccessorType(XmlAccessType.FIELD)
public class NodeMap {

    private Map<Integer, Node> nodeMap = new HashMap<Integer, Node>();

    public Map<Integer, Node> getNodeMap() {
        return nodeMap;
    }

    public void setNodeMap(Map<Integer, Node> employeeMap) {
        this.nodeMap = employeeMap;
    }
}
