package entity;

/**
 * Created by irina on 25.04.16.
 */
public class Count implements Comparable<Object>{
    private int a = 0;

    public Count(int i) {
        a=i;
    }

    public void add(){
        a++;
    }
    public int getA(){
        return a;
    }

    @Override
    public int compareTo(Object o) {
        if (this.a == ((Count) o).a){
            return 0;
        }
        else
        if (this.a > ((Count) o).a){
            return 1;
        }
        else
            return -1;
    }
}
