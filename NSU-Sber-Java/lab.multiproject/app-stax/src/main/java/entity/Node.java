package entity;

/**
 * Created by irina on 25.04.16.
 */
public class Node {
    public Node(String name, Count count) {
        this.name = name;
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Count getCount() {
        return count;
    }

    public void setCount(Count count) {
        this.count = count;
    }

    private String name;
    private Count count;

}
