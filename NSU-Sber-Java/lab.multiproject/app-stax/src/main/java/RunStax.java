import marshaller.Marshalling;
import stax.StAXParser;

import java.util.logging.Logger;

/**
 * Created by irina on 25.04.16.
 */
public class RunStax {
    private static StAXParser parser;
    private Marshalling marshaller;

    public RunStax() {
        parser = new StAXParser();
    }
    public static void main(String [] argc){
        Logger log = Logger.getLogger(RunStax.class.getName());
        log.info("StAX parser connecting");
        RunStax lab = new RunStax();
        log.info("Parser starting");
        lab.parse();
        log.info("Parsing done");
        log.info("Ordering map");
        lab.ordering();
        log.info("Marshalling map to xml file");
        lab.marshalling();
    }
    static void parse(){
            parser.parse("RU-NVS.osm");
            //parser.printList(parser.getMap());

    }
    static void ordering(){
        parser.sortByValue(parser.getMap());
        parser.printList(parser.sortByValue(parser.getMap()));
    }

    public void marshalling(){
        marshaller = new Marshalling(parser.getMap());
        marshaller.marshalling();
    }
}
